﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Laptop
/// </summary>
public class Laptop
{
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string laptopBrand { get; set; }
    public string paymentMethod { get; set; }
    public bool newsletter { get; set; }
    public int Quantity { get; set; }



    public Decimal Calculate(decimal price)
    {

        decimal totalbill = price * Quantity;

        return totalbill;
    }




    public Laptop()
    {
        firstName = "john";
        lastName = "doe";
        labtopBrand = "Apple 3";
        paymentMethod = "cash";
        newsletter = true;
        Quantity = 1;

    }
}
