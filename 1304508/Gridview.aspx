﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Gridview.aspx.cs" Inherits="Gridview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 620px;
        }
        .auto-style3 {
            width: 620px;
            height: 23px;
            text-align: center;
            background-color: #CC3300;
        }
        .auto-style4 {
            height: 23px;
            background-color: #CC3300;
        }
        .auto-style5 {
            font-size: x-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">&nbsp; <strong><span class="auto-style5">VIEW INVOICE</span></strong></td>
                <td class="auto-style4"></td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:1304508ConnectionString %>" DeleteCommand="DELETE FROM [Invoice] WHERE [InvoiceNumber] = @InvoiceNumber" InsertCommand="INSERT INTO [Invoice] ([ID], [ProductID], [Quantity], [TotalCost]) VALUES (@ID, @ProductID, @Quantity, @TotalCost)" SelectCommand="SELECT * FROM [Invoice]" UpdateCommand="UPDATE [Invoice] SET [ID] = @ID, [ProductID] = @ProductID, [Quantity] = @Quantity, [TotalCost] = @TotalCost WHERE [InvoiceNumber] = @InvoiceNumber">
                        <DeleteParameters>
                            <asp:Parameter Name="InvoiceNumber" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="ID" Type="Int32" />
                            <asp:Parameter Name="ProductID" Type="Int32" />
                            <asp:Parameter Name="Quantity" Type="Int32" />
                            <asp:Parameter Name="TotalCost" Type="Decimal" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ID" Type="Int32" />
                            <asp:Parameter Name="ProductID" Type="Int32" />
                            <asp:Parameter Name="Quantity" Type="Int32" />
                            <asp:Parameter Name="TotalCost" Type="Decimal" />
                            <asp:Parameter Name="InvoiceNumber" Type="Int32" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="InvoiceNumber" DataSourceID="SqlDataSource1">
                        <Columns>
                            <asp:BoundField DataField="InvoiceNumber" HeaderText="InvoiceNumber" InsertVisible="False" ReadOnly="True" SortExpression="InvoiceNumber" />
                            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                            <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                            <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" SortExpression="TotalCost" />
                        </Columns>
                    </asp:GridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
