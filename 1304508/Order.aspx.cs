﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Order : System.Web.UI.Page
{
    public  _labtop = new Laptop();
  
    protected void Page_Load(object sender, EventArgs e)
    {

    }

   
    protected void Button1_Click(object sender, EventArgs e)
    {

     
   _labtop.firstName = txtFname.Text;
   
        _labtop.lastName = txtLname.Text;
        _labtop.newsletter = chkNewsletter.Checked;
        _labtop.paymentMethod = rblPayment.SelectedValue;
        

        int amount;
        {
        if (int.TryParse(txtQuantity.Text, out  amount) == false)

            amount = 0;

        }
        _labtop.Quantity = amount;


        _labtop.labtopBrand = ddlLabtop.SelectedItem.Text;
    decimal val = Convert.ToDecimal( ddlLabtop.SelectedItem.Value);
        Session["fname"] = _labtop.firstName;
      Session["lname"] = _labtop.lastName;
      Session["brand"] = _labtop.labtopBrand;
      Session["quantity"] = _labtop.Quantity;
      Session["news"] = _labtop.newsletter;
      Session["pay"] = _labtop.paymentMethod;
      Session["cost"] = _labtop.Calculate(val);
      BusinessClass newBusiness= new BusinessClass();
  decimal total = _labtop.Calculate(val);
      newBusiness.InsertCustomer(_labtop.firstName,_labtop.lastName,_labtop.newsletter);
   newBusiness.AddInvoice(6,4,_labtop.Quantity,total);
       
        Response.Redirect("~/Display.aspx");

    }
}