﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Display : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        lblFname.Text= Session["fname"].ToString();
        lblLname.Text=Session["lname"].ToString();
        lblPayment.Text=Session["pay"].ToString();
        lblQuantity.Text=Session["quantity"].ToString();
        lblBrand.Text=Session["brand"].ToString();
        lblcost.Text = Session["cost"].ToString();
        
       
    }
    protected void Button3_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/formview.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/GridView.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Listview.aspx");
    }
}