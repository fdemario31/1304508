﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="formview.aspx.cs" Inherits="formview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: xx-large;
            font-weight: bold;
            background-color: #CC3300;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Customer</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="CustID" DataSourceID="SqlDataSource1">
                        <EditItemTemplate>
                            CustID:
                            <asp:Label ID="CustIDLabel1" runat="server" Text='<%# Eval("CustID") %>' />
                            <br />
                            FirstName:
                            <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                            <br />
                            LastName:
                            <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                            <br />
                            Newsletter:
                            <asp:CheckBox ID="NewsletterCheckBox" runat="server" Checked='<%# Bind("Newsletter") %>' />
                            <br />
                            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            FirstName:
                            <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                            <br />
                            LastName:
                            <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                            <br />
                            Newsletter:
                            <asp:CheckBox ID="NewsletterCheckBox" runat="server" Checked='<%# Bind("Newsletter") %>' />
                            <br />
                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            CustID:
                            <asp:Label ID="CustIDLabel" runat="server" Text='<%# Eval("CustID") %>' />
                            <br />
                            FirstName:
                            <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' />
                            <br />
                            LastName:
                            <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' />
                            <br />
                            Newsletter:
                            <asp:CheckBox ID="NewsletterCheckBox" runat="server" Checked='<%# Bind("Newsletter") %>' Enabled="false" />
                            <br />

                        </ItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:1304508ConnectionString %>" SelectCommand="SELECT * FROM [Customer]"></asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
