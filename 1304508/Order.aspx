﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Order.aspx.cs" Inherits="Order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 324px;
        }
        .auto-style4 {
            height: 23px;
            width: 324px;
        }
        .auto-style5 {
            width: 433px;
        }
        .auto-style6 {
            height: 23px;
            width: 212px;
        }
        .auto-style7 {
            width: 212px;
        }
        .auto-style8 {
            height: 23px;
            width: 212px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: xx-large;
            background-color: #CC3300;
        }
        .auto-style9 {
            height: 23px;
            background-color: #CC3300;
        }
        .auto-style10 {
            height: 23px;
            width: 324px;
            background-color: #CC3300;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <div>
    
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style10">&nbsp;</td>
                        <td class="auto-style8">
                            Computer Sales</td>
                        <td class="auto-style9">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style4">&nbsp;</td>
                        <td class="auto-style6">
                            &nbsp;</td>
                        <td class="auto-style2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style4">FIRST NAME</td>
                        <td class="auto-style6">
                            <asp:TextBox ID="txtFname" runat="server" Width="183px"></asp:TextBox>
                        </td>
                        <td class="auto-style2">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ErrorMessage="Name is required" ForeColor="Red" ControlToValidate="txtFname"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                     <tr>
                        <td class="auto-style3">LAST NAME</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txtLname" runat="server" Width="181px"></asp:TextBox>
                         </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Name is required" ForeColor="Red" ControlToValidate="txtLname"></asp:RequiredFieldValidator>
                         </td>
                    </tr>
                    <tr>
                        <td class="auto-style4">LAPTOP BRAND</td>
                        <td class="auto-style6">
                            <asp:DropDownList ID="ddlLabtop" runat="server">
                                <asp:ListItem Value="55000"> HP G71</asp:ListItem>
                                <asp:ListItem Value="50000">Sony H2</asp:ListItem>
                                <asp:ListItem Value="70000">Apple 3</asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        <td class="auto-style2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style3">QUANTITY</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txtQuantity" runat="server" Width="179px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Quantity is required" ControlToValidate="txtQuantity" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">PAYMENT<br />
                            METHOD</td>
                        <td class="auto-style7">
                            <asp:RadioButtonList ID="rblPayment" runat="server">
                                <asp:ListItem>Cash</asp:ListItem>
                                <asp:ListItem>CreditCard</asp:ListItem>
                                <asp:ListItem>DebitCard</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="payment method not selected" ControlToValidate="rblPayment" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">SIGN UP FOR<br />
                            NEWSLETTER</td>
                        <td class="auto-style7">
                            <asp:CheckBox ID="chkNewsletter" runat="server" OnCheckedChanged="chkNewsletter_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
    
        </div>
        <p>
        </p>
        <table class="auto-style1">
                    <tr>
                        <td class="auto-style5">
                            <asp:Button ID="btnSubmit" runat="server" OnClick="Button1_Click" Text="SUBMIT" Width="113px" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
    </form>
                </body>

</html>
