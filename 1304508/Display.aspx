﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Display.aspx.cs" Inherits="Display" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 286px;
        }
        .auto-style3 {
            width: 286px;
            height: 23px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: xx-large;
            background-color: #CC3300;
        }
        .auto-style6 {
            width: 286px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: xx-large;
            background-color: #CC3300;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style6">&nbsp;</td>
                <td class="auto-style5">
                    Display</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">First Name</td>
                <td>
                    <asp:Label ID="lblFname" runat="server" Text="lblFname"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Last Name</td>
                <td>
                    <asp:Label ID="lblLname" runat="server" Text="lblLname"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;Laptop Brand&nbsp;</td>
                <td>
                    <asp:Label ID="lblBrand" runat="server" Text="lblBrand"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Quantity</td>
                <td>
                    <asp:Label ID="lblQuantity" runat="server" Text="lblQuantity"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Payment method</td>
                <td class="auto-style4">
                    <asp:Label ID="lblPayment" runat="server" Text="lblPayment"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Total Cost</td>
                <td>
                    <asp:Label ID="lblcost" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Button1" runat="server" Text="Home" Width="102px" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Grid view" Width="103px" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Form View" Width="101px" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" style="height: 26px" Text="List View" Width="100px" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
</body>
</html>
