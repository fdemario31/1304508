﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Collections;

[DataObject(true)]
public class BusinessClass
{
    public BusinessClass()
    { }
     
          [DataObjectMethod(DataObjectMethodType.Select)]
    public IEnumerable GetCustomersById(int customerId)
    {
        SqlConnection con = new SqlConnection(GetConnectionString());
        string storedProcedureName = "spSelectCustomer";
        SqlCommand cmd = new SqlCommand(storedProcedureName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@customerId", SqlDbType.Int).Value = customerId;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        return dr;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public IEnumerable DeleteCustomersById(int customerId)
    {
        SqlConnection con = new SqlConnection(GetConnectionString());
        string storedProcedureName = "spDeleteCustomer";
        SqlCommand cmd = new SqlCommand(storedProcedureName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@customerId", SqlDbType.Int).Value = customerId;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        return dr;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public void InsertCustomer( string firstname, string lastname,bool newsletter)
    {
    
        SqlConnection con = new SqlConnection(GetConnectionString());
        string storedProcedureName = "spInsertCustomer";

        SqlCommand cmd = new SqlCommand(storedProcedureName, con);

        cmd.CommandType = CommandType.StoredProcedure;

        //cmd.Parameters.AddWithValue("@customerId", customerId);
        cmd.Parameters.AddWithValue("@firstName", firstname);
        cmd.Parameters.AddWithValue("@lastName", lastname);
      
      //  cmd.Parameters.AddWithValue("@PaymentMethod", paymentMethod);
     //   cmd.Parameters.AddWithValue("@totalBill", totalBill);
        cmd.Parameters.AddWithValue("@newsletter", newsletter);
        con.Open();
         cmd.ExecuteNonQuery();
        
        con.Close();
    }






    [DataObjectMethod(DataObjectMethodType.Insert)]
    public void AddInvoice(int cusid,int productID ,int quantity, decimal total)
    {

        SqlConnection con = new SqlConnection(GetConnectionString());
        string storedProcedureName = "spAddInvoice";

        SqlCommand cmd = new SqlCommand(storedProcedureName, con);

        cmd.CommandType = CommandType.StoredProcedure;

     

        cmd.Parameters.AddWithValue("@id", cusid);
          cmd.Parameters.AddWithValue("@productId", productID);
       
        cmd.Parameters.AddWithValue("@Quantity", quantity);

     
        cmd.Parameters.AddWithValue("@totalbill",total);
        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    }










  
    [DataObjectMethod(DataObjectMethodType.Update)]
    public void UpdateCustomer(/*int customerId,*/ string firstname, string lastname,  string paymentMethod,  bool newsletter)
    {

        SqlConnection con = new SqlConnection(GetConnectionString());
        string storedProcedureName = "spUpdateCustomer";

        SqlCommand cmd = new SqlCommand(storedProcedureName, con);

        cmd.CommandType = CommandType.StoredProcedure;

       // cmd.Parameters.AddWithValue("@customerId", customerId);
        cmd.Parameters.AddWithValue("@firstName", firstname);
        cmd.Parameters.AddWithValue("@lastName", lastname);
        cmd.Parameters.AddWithValue("@PaymentMethod", paymentMethod);
       // cmd.Parameters.AddWithValue("@totalBill", totalBill);
        cmd.Parameters.AddWithValue("@newsletter", newsletter);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
    }

    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["1304508ConnectionString"].ConnectionString;
    }


    public static string IdGenerator()
    {

        char[] letters = "1234567890qwertyuiopasdfghjklzxcvbnm".ToCharArray();
        Random rand = new Random();
        string id = "";
        for (int i = 0; i < 7; i++)
         {

             id += letters[rand.Next(0, 35)].ToString();
        
          }


        return id;
    }


	}
